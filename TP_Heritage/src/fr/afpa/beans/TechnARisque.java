package fr.afpa.beans;


public final class TechnARisque extends Technicien implements Risque {

	public TechnARisque(String nom, String prenom, int age, String dateEntree, int unite) {
		super(nom, prenom, age, dateEntree, unite);
	}

	public TechnARisque() {}

	@Override
	public double prime() {
		super.setSalaire(super.calculerSalaire() + 200);
		return super.getSalaire();
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString() + " est un technicien � risque";
	}

}
