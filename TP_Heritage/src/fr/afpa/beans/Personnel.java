package fr.afpa.beans;


public class Personnel {

	private Employee[] collectionEmployee;
	private double salaireMoy;
	private int indice;
	
	
	public Personnel() {
		collectionEmployee =  new Employee[6];
	}
	
	public void ajouterEmploye(Employee employee) {
		collectionEmployee[indice] = employee;
		indice++;
	}
	
	public double salaireMoyen() {
		for (int i = 0; i < collectionEmployee.length; i++) {
			if (collectionEmployee[i] != null) {
				
				salaireMoy = salaireMoy + collectionEmployee[i].getSalaire();
			}
		}
		salaireMoy = salaireMoy/collectionEmployee.length;
		return salaireMoy;
	}
	
	public void afficherSalaires() {
		for (int i = 0; i < collectionEmployee.length; i++) {
			if (collectionEmployee[i] != null) {
				collectionEmployee[i].calculerSalaire();
				System.out.println("Le salaire de l'employ� " + collectionEmployee[i].getPrenom()  + " " + collectionEmployee[i].getNom() + " est : " + collectionEmployee[i].getSalaire() + "�");	
			}
		}
	}
	
	
	public Employee[] getCollectionEmployee() {
		return collectionEmployee;
	}
	
	public double getSalaireMoy() {
		return salaireMoy;
	}
	
	public void setCollectionEmployee(Employee[] collectionEmployee) {
		this.collectionEmployee = collectionEmployee;
	}
	
	public void setSalaireMoy(double salaireMoy) {
		this.salaireMoy = salaireMoy;
	}
	
	
}
