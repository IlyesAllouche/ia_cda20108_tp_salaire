package fr.afpa.beans;

public final class ManutARisque extends Manutentionnaire implements Risque {

	public ManutARisque(String nom, String prenom, int age, String dateEntree, int heures) {
		super(nom, prenom, age, dateEntree, heures);
	}

	public ManutARisque() {}

	
	@Override
	public double prime() {
		super.setSalaire(super.calculerSalaire() + 200);
		return super.getSalaire();
	}
	
	@Override
	public String toString() {
		return super.toString() + " est un manutentionnaire � risque";
	}
	
	

}
