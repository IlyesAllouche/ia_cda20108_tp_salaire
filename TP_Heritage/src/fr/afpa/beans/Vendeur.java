package fr.afpa.beans;


public class Vendeur extends Employee {
	
	private int chiffreAffaire;

	public Vendeur(String nom, String prenom, int age, String dateEntree, int CA) {
		super(nom, prenom, age, dateEntree);
		this.chiffreAffaire = CA;
	}

	public Vendeur() {}
	
	@Override
	public double calculerSalaire() {
		super.setSalaire(chiffreAffaire*0.2 + 400);
		return super.getSalaire();
	}

	public double getChiffreAffaire() {
		return chiffreAffaire;
	}

	public void setChiffreAffaire(int chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	@Override
	public String toString() {
		return "Le vendeur " + super.getNom() + " " + super.getPrenom();
	}

	
	
}
